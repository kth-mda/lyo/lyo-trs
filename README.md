KTH-OFFIS TRS project code
==============================================================================

This repository contains the code developed by KTH and OFFIS during the KTH-OFFIS project collaboration.

**See [docs/](docs/)**


## License

> Copyright (c) 2023 KTH Royal Institute of Technology and others
> 
> This program and the accompanying materials are made available under the 
> terms of the Eclipse Public License 2.0 which is available at
> http://www.eclipse.org/legal/epl-2.0.
> 
> SPDX-License-Identifier: EPL-2.0

This project uses code from the Eclipse Lyo project dual-licensed under `EPL-1.0 OR BSD-3-Clause`. We chose to use that code under the terms of `EPL-1.0`.

> Copyright (c) 2012 IBM Corporation and Contributors to the Eclipse Foundation.
> 
> All rights reserved. This program and the accompanying materials
> are made available under the terms of the Eclipse Public License v1.0
> and Eclipse Distribution License v. 1.0 which accompanies this distribution.
>  
> The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
> and the Eclipse Distribution License is available at
> http://www.eclipse.org/org/documents/edl-v10.php.
> 
> SPDX-License-Identifier: EPL-1.0 OR BSD-3-Clause
TRS infrastructure setup
==============================================================================

## Setup of a Web server

An application web server is needed for deploying the adapter applications. Until now, two
web servers have been tested. With the following remarks:

- Tomcat: the matlab executable can not be called from inside apache tomcat for some reason which prevents the functionality
of the matlab adapter. Accordingly Jetty was tried next

- Jetty: Jetty supports several customization options e.g. the size of the content of the POST request.
At some point the update requests sent by the TRS client to the triplestore were rejected because the limit on the content 
size of the POST requests was too small. Accordingly, this limit needs to be enlarged to insude the correct functionality.

### Web application deployment
To deploy the web application on Jetty, copy the web application archive (.war file) to the web applications directory and restart the server


## Setup of the Triplestores

2.2.1 Setup of openrdf sesame

Sesame comes in the form of two war files:

openrdf workbench for the administration operations of the server
openrdf sesame which ofers the sparql http endpoints to access a repository via http using sparql

as in 2.1.1. the two web applications need to be deployed to the web application server chosen.

From the openrdf workbench application a repository needs to be created. In general, there are two
main types of repositories. One of them is the Native java store which will persist through restarts
of the web application / web server. The second is the in memory store which will not persist.

To create a new repository select "New Repository" under "Repositories" and choose a type and a name
and carry on with the creation.

The two SPARQL endpoints of the created repository are the following

sparql update endpoint: %oepnrdfsesamewebappurl%/repositories/%reponame%/statements

sparql query endpoint: %oepnrdfsesamewebappurl%/repositories/%reponame%/

			
## Setup of the OSLC adapters

### Configuration of the OSLC adapter
Start by copying the exampe appdatafolder provided with the web application war file to the needed location.
Depending on the OS you are using this is the location the app data folder you're provided with should be moved 2 i.e.
the new path to the folder you are provided with should become one of the following
	
- `%USER_HOME%/.%APPNAME%` for *nix
- `%APPDATA%/%APPNAME%` for Windows
- `%USER_HOME%/Library/Application Support/%APPNAME%` for macOS

E.g., in the case of the bugzilla adapter the windows appdata directory is the following
	
	C:\Users\bob\AppData\Roaming\oslc4jbugz
	
where `bob` is the name of the user running the web application server software.
	
### Setup of the Simulink Adapter

A. The war file needs to be deployed on the application web server first as described in 2.1.1

B. The configuration files need to be prepared and copied to the appropriate directories as specified in 
2.1.2. A minimal configuration is needed. Please folllow the steps in chapter 5 of the 
"Instructions to install and execute the OSLC Simulink Adapter.pdf" document provided with this tutorial
in order to configure the adapter as required.
After finishing the configuration matlab needs to be set up as specified in chapter 6 of the same document.

### Setup of the Bugzilla Adapter

A. The war file needs to be deployed on the application web server first a	s described in 2.1.1

B. The configuration files need to be prepared and copied to the appropriate directories as specified in 
2.1.2. A minimal configuration is needed. Please folllow the steps in the "Edit the Bugzilla Configuration"
of https://wiki.eclipse.org/Lyo/BuildTRS4JBugzilla to perform the needed configuration steps.

C. The tutorial for the bugzilla adapter suggests that the adapter should be able to opreate with all versions
of bugzilla starting from 4.1 however that is not true. The latest version of bugzilla with which the 
adapter is tested and works is 4.2.15


---
title: Getting Started with the TRS Consumer utility
author: Omar Kacimi, Andrii Berezovskyi
---

# Motivation

The *TRS Consumer * utility uses th TRS interfaces of existing OSLC adapters in order to cache the data exposed by these OSLC adadapters in a preconfigured HTTP SPARQL enabled triplestore and to keep this data cache up to date with the data coming from the adapters. for more information please check the  [OSLC TRS 2.0 draft spec][1]

# Supported features

The following features are currently supported:

* Full processing of the TRS information of a TRS provider

* Concurrent processing of Base Members of a Tracked Resource Set

* Concurrent processing of Change Events

* Concurrent processing of TRS Providers

* Basic Http Authentication support

The implementation of the following features is planned:

* Concurrent retrieval of Base and ChangeLog resources from TRS Providers

* Support of OAuth authentication

* Management of Server Rollback to an earlier state

# Starting up

1. Download Eclipse for JEE development and start it
2. Clone the Lyo Git Repo located under:
3. Import the org.eclipse.lyo.oslc4j.trs.consumer project into your eclipse workspace.
3. Under Run Configurations in Eclipse select TRS Consumer build verify clean install and run it. This builds the TRS Consumer binary under the target folder in the project the binary.
4. Configure the TRS Consumer as mentioned in the [config](#config) chapter.
5. Run the TRS Consumer fro the Launch configuration of the TRS Consumer using the TRs Consumer launch Java run Configuration

# TRS Consumer configuration <a name="config"></a>

The Application data for the TRS Consumer is stored in the application data folder. The application data folder has the following form in each operating system

	%USER_HOME% is the user home dir in this case

	A. UNIX

	%USER_HOME%/.%APPNAME%

	B. Windows

	%APPDATA%/%APPNAME%

	C. Mac

	%USER_HOME%/Library/Application Support/%APPNAME%

For example on windows:

	C:\Users\Omar\AppData\Roaming\trsclient

for the TRS Consumer software the configuration consists of the following:

- A TRSConsumer properties file
- a TRS providers folder containing a configuration file for each TRS Provider connected to the client

The TRS client properties file contains the urls of the sparql http endpoint
sof the triplestore used as lifecycle data backend.If the triplestore uses
basic http authentication, the basic authentication credentials are included
in this configuration file. The configuration file has the following form:


sparqlUpdateBase=https://vservices.offis.de/rtp/fuseki/v1.0/ldr/update

sparqlQueryBase=https://vservices.offis.de/rtp/fuseki/v1.0/ldr/query
baseAuth_user=okacimi
baseAuth_pwd=


[1]: http://open-services.net/wiki/core/TrackedResourceSet-2.0/

Extending an existing OSLC adapter with a TRS interface
==============================================================================

# Motivation

The purpose of the *TRS Provider* library is to provide a developer with a ready
to use set of classes over which he can provide a minimal implementation that
will result in a TRS interface with minimal effort.

# Overview

The TRS interface consists mainly of two classes:

- `ChangeHistories`
- `TrackedResourceSetService`

`ChangeHistories` class represents the backbone of the TRS interface and
manages the TRS artifact and the objects representing the history information
served through TRS.

`TrackedResourceSetService` class manages the rest calls to the TRS service
e.g. a REST HTTP GET request for the TRS of the adapter, or an HTTP GET request
for some page of the change log or the base.

In order to implement a TRS interface two classes need to be implemented, each
one of these classes should respectively implement one of the classes above.

## Implementation note

Change events inside the Change Log can lose ordering in the response that is
served to the clients. This will not be fixed due to the following:

- there are no requirements on how the ordering of change events is reflected on the rdf model
- the change events inside a change log are not elements of any rdf list concept so there is no way of reflecting in the rdf model of the change Log the order of the change events.
- The only requirements in TRS is that the change log segmentation respects the ordering which is the case here.

See https://gitlab.com/assume/offis-svn/issues/5 for more information.

# Use case

To explain how this functionality would work let's exemplify an implementation
of a TRS interface for a *ReqIf Adapter*. The TRS interface would be
implemented using the two following classes:

1. `ReqIfChangeHistories` extends `ChangeHistories`.
1. `ReqIfTrackedResourceSetService` extends `TrackedResourceSetService`

`ReqIfChangeHistories` class would need to provide an implementation of the
abstract method `public HistoryData[] getHistory(HttpServletRequest
httpServletRequest, Date dateAfter)`. In this method, the implementer of the
class handles returning the history information for the resources handled by
the adapter. The history information is returned as a list of instances of the
intuitive `HistoryData` class.

Please not that it is highly recommanded to instantiate the subclass ReqIfChangeHistories
as a singleton. In the initial API, the idea was to enable having the subclasses 
of the ChangeHistories class only as singletons. However, since this seems to be 
currently problematic to implement in Java. It shall for now be a recommandation
for the instantiation of these subclasses and to be adressed soon. An example
of a thread safe singleton implementation of the ChangeHistories class is provided
in the simulink adapter's TRS interface.


`ReqIfTrackedResourceSetService` class needs to provide an implementation
of the two following methods:

1. `@Override protected String getServiceBase()`. This method provides the HTTP
URL prefix under which the TRS artifacts are served e.g.
`http://uri/trs/changeLog/pageNumber`.

1. `@Override protected ChangeHistories getChangeHistories()`. This method
provides an instance of the class extending the `ChangeHistories` class, in this
case `ReqIfChangeHistories`. Again, the ReqIfChangeHistories class needs not to
be instantiated more than once and should follow the singleton pattern as shown
in the example of the simulink's TRS adapter.

# Implementation in ProR Adaptor

A dependency for the TRS Provider library was added:

    <dependency>
        <groupId>org.eclipse.lyo.oslc4j.trs.provider</groupId>
        <artifactId>org.eclipse.lyo.oslc4j.trs.provider</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>

2 classes were created:

1. `se.kth.mda.assume.prordemo.servlet.ReqifChangeLog`
1. `se.kth.mda.assume.prordemo.services.ReqifTrsService`

The `ReqifTrsService` was registered in the static constructor of the
`Application` class:

    // TRS
    RESOURCE_CLASSES.add(ReqifTrsService.class);

`ReqifChangeLog` provides the OSLC TRS Change Log as an array of `HistoryData`
objects. `ReqifTrsService` responds to the HTTP requests according to [OSLC TRS
2.0 draft spec][1].


[1]: http://open-services.net/wiki/core/TrackedResourceSet-2.0/
the transaction specification by SPARQL updates isn't in the proper sense of the term specifying transactions
 but rather atomicity of the set of operations in the request. Unfortunately the behaviour in case of e.g. concurrency
 is federated to the internal implmentation of the SPARQL service.
 
 https://www.w3.org/TR/sparql11-update/#graphStore:2.2 SPARQL 1.1 Update Services
 In the implementation of the lifecycle repository, this will be considered by treating every change event as a 
 single update request to be able to provide useful information about the extent to which the change events have 
 been processed.
 
 Out of Memory exception with more than 2000 base elements
Add creation events about some of the elements of the adapter
/
fixed by adding the -Xss104k jvm argument
/
From the Simulink Server side
	
	- add some artifical data
		- specify as arguments the limits of how much to
	
	- think about whether this is the right way to operate from the clients side
		- processing all the change events and the base at once.
		- add limit of how many change events are to be processed each time and save the last processed one to restart from it

	- add a small script somewhere to regularly add artificial change data for each one of the existing resources.
 
 
 /
 Corrected
 The current implementation seems not to support Deletion events. This is to be kept in mind in case we are planning to 
 use the current implementation as a basis for a generic TRS implementation
 
 /
 
 The current implementation does not support sparql endpoints including authentication mechanisms. To be investigated.
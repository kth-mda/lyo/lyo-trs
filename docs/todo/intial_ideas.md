what kind of funcitonality to be provided by lifecycle tool
		  
	- Query provider
		- Provided functionality ?
			- Overview of functionality provided by IBM RELM
				- from: https://jazz.net/help-dev/clm/index.jsp?re=1&topic=/com.ibm.team.jp.relm.doc/topics/c_node_using_product.html&scope=null
					- Views giving an overview of selected artifacts types and the artifacts 
					  related to them
					- Special views ( marketing name: impact analysis) showing elements related
					  to a certain element with the possibility to filter displayed
					  traced elements and their properties
					- Integration with PTC Windchill PLM
					- misc
						- " To create a report with different output, such as bar charts and Gantt 
						charts, you need Rational Publishing Engine. Rational Publishing Engine is not 
						included with Rational Engineering Lifecycle Manager; separate installation and
						license are required "
						- LQE and RELM overview: https://jazz.net/help-dev/clm/index.jsp?re=1&topic=/com.ibm.team.jp.relm.doc/topics/c_node_using_product.html&scope=null
						- LQE using configuration management
							- * are we including as a feature somehow in our work ?
					
			- Minimal browser for visualizing the results of a query ?
			- Visualization of the data by OSLC domain with link information ?
			- Impact Analysis
				- in which sense ?
				- provide elements of some type that would be impacted ( which are linked )
				  to the element which is of interest
				  - be able to see the impact of the change propagated through the different lifecycle
				  domains.
				- what is the novelty in regards to what is provided by RELM.
					- Contract-based impact analysis ?
			- Example scenarios to be supported
				o	Scenario 2
					?	Automation of Running Verifying Activities until they are successfully ran
							Steps
							o	Manual: Creation of Verification activities linked to model artifacts
							o	Automation support:
								?	Change Impact Support Tool
										Run verification activities
										o	New ones?
										o	Poll Adapter of RQM for new artifacts?
										For all failed runs, generate an automated change request
									linked to the verification activity
										Once the change requests are updated, run the verification 
									activities again
										Repeat the process
							Useful queries provided by lifecycle tool
							o	For some failed verification activity, are all the change requests
							updated from new to done and have the corresponding model elements been 
							changed since the creation of the corresponding change requests
						 Scope restricted to Simulink and Bugzilla
							- TRS Adapter of Simulink exposes the status of the resources in the adapter
							- TRS Adapter of Bugzilla exposes the status of the resources in the adapter
							
							- Flow
								1. The impact analysis tool polls a qm TRS provider for status of resources
								2. At the creation detection the tool sends an automation request to run the
								  created activity
								3. At some poll of the next TRS the status 
								
				o	Scenario 1
					?	Support with the exhaustiveness of the design process
							Purpose: help the engineer to insure traceability of 
						all elements before starting with the verification process
							How: enable querying for all the design artifacts that
						have not yet been linked
							o	E.g. 
								?	Safety-related Requirements not yet linked to 
								Architecture Elements
								?	Safety-related Requirements linked to Architecture
								Elements not yet linked to a verification activity
				


		- Architecture
			- prototype
				- a triple store running behind the scenes
				- a web application logic
					- SPARQL query service base
				- a TRS client part
					- 
				- optionally a server application
					- hosting the web application that can be launched directly on amachine as ane executable
			- options
				- use a triplestore with built-in support for SPARQL queries processing over http
					- advantages are possibility to substitute any kind of such interface in the setup
					  to plug it with the TRS Client.
					- Requirements on triplestore
						- SPARQL endpoint through http
							- support for
								- Queries including selection and update
				- a triplestore running behind the scenes
					- criteria
						- ease of deployment
							- Marmotta performance tuning is hard to do
							- 
						- documentation
						- scalability ? 
							- http://stackoverflow.com/questions/4921407/triplestore-for-large-datasets
								- 4store
								- Virtuoso
						- migration ?
						- open source and eclipse compatible
							- Sesame ? Eclipse project 2 b 
							- Fuseki along TDB as main suspects ? Apache, hence compliant
						- performance requirements ?
							- deployable on a minimal server machine ?
						- being able to use ready to use functionality for POJO to Jena 
						- decent performance
						- Support of emerging Query languages ?
							- SeRQL
							- SESAME seems to support 
						- Support for SPARQL
						- Support of OSLC Query language optional
						- Compatible with the technology stack of OSLC4J if possible
							- can Jena deal with models that come from a different RDF
							  triplestore than one from Jena
					- big names
						- Jena TDB
							- Fuseki
						- Sesame
						- Apache marmotta
							- Kiwi
							- documentation not available or rare..
						- Parliament
						- Open Anzo
						- Mulgara
						- cayley
						- brightstardb
						
						
					- clear candidates
						- not recent benchmarks prefer sesame in term of performance
							- http://www.it.uu.se/research/group/udbl/Theses/Shridevika.MaharajanMSc.pdf
								- it seems that the information might be slightly outdates
								because for example Jena TDB is the next step from SDB
							- file:///C:/Users/okacimi/Downloads/ontoquad_CTI.pdf
								- Jena is not so far behind, licensing problem again with the others
							- https://www.aaai.org/ocs/index.php/AAAI/AAAI12/paper/viewFile/5168/5384
								- !The performance of Jena is very low with full data set in place
								- with the increase of the dataset size to 50% sesame performance increases
								- for 10% dataset Jena wins
								- Overall the performance of JEna is better
							- http://ribs.csres.utexas.edu/nosqlrdf/nosqlrdf_iswc2013.pdf
								- the combination of triple stores and SPARQL query processors
								there are using are not standard and will probably not be well supported.
							
							- http://wifo5-03.informatik.uni-mannheim.de/bizer/berlinsparqlbenchmark/results/V7/index.html#resultsExplore
							- http://stackoverflow.com/questions/4921407/triplestore-for-large-datasets
							- https://www.w3.org/wiki/LargeTripleStores#Jena_TDB_.281.7B.29
							- http://answers.semanticweb.com/questions/884/rdf-store-comparison
							
							- http://blog.datagraph.org/2010/04/rdf-nosql-diff
							
							- expired
								- http://answers.semanticweb.com/questions/1638/jena-vs-sesame-is-there-a-serious-complete-up-to-date-unbiased-well-informed-side-by-side-comparison-between-the-two
						- candidates
							- Jena TDB for compatibility with technology stack
							- Sesame as eclipse project and used in OSLC Lyo RIO
								- comparison
									- http://db-engines.com/en/system/Jena%3BSesame
									- http://web-semantique.developpez.com/tutoriels/francart/sesame-jena-comparaison-fonctionnalites/
									- http://blog.sparna.fr/2012/05/08/rdf-sesame-jena-comparaison-des-fonctionnalites/
									
						- 
						
					- eliminated
						- allegro ( closed source)
						- Virtuoso ( open source version available under GPL V2 )
						- 4 store (GPL v2)
						- Blazegraph - BigData (GPL v2)
						- GraphDB-Lite (closed source)
						- Stardog ( onlysome components of it are open source and licensing scheme not clear )
						- OWLIM-Lite( closed source)
						- RDFox (in memory, needs lot of RAM and license not clearly whether eclipse compatible)
						- Smart-M3 ( not enough information)
						- OpenDFKI RDFBroker(GPL)
						- Semafora (seems 2 b commercial)
						- Berkeley DB ( AGPL)
						- Oracle Spatial and Graph RDF Semantic Graph( 
						- ldpath ( merged into marmotta)
						- Spin ( seems like a standard rather than a triplestore ?)
						
					

					
					- some stackoverflow threads from 2011 recommand for large data sets and java use
						- OpenLinkVirtuose not clear whether it's open source or not
						- 4 store
					
					- Types of triplestores and their uses
						- 
					- use cases
						- Support running verification activities automatically
						   with the Lifecycle tool
						- Support with insuring the establishment of traceability between all artifacts
					
					- available triplestores for OSLC4J
						- it seems that RIO is using sesame in one of its projects
						  not sure how that is supposed to align with the use of JEna elsewhere ?

			- update the underlying model every time there is a resource update in the database
				- Flow
					-  Initialization of the index
						- query for the TRS
						- two parallel threads
							- threads
								1. query for the list of the members of the base and pupulate local list
									- populate the sync event
								2. go up the change log looking for the cutoff event
									- in case this change event can not be retrieved. 
									  Fail to retrieve the resource set.
							
							- threads interaction
								- thread 1 populates the sync event
								- thread 2 relies on the sync event as a stopping criteria
						- outcome:
							- index either up 2 date or failure
							- retry after the update interval period
							- In case of success, maintain the last 10 processed change events
				
					- Update of the index
						- retrieve the change log
						- load all events post the last event processed during previous update
						A. if the last event can be found process the series of events against the base
						  and reload the update coutner
						- in case the last event processed during the update can't be found. 
							- Compare some of the previous ones from the most recent to the oldest
							  with the cutoff event of the current base.
								- If some event matches unprocess all the change events processed alst time
								that were post this event
									- modification events can be undone by retrieving the most recent version of the modified resource
									- creation events are rolled back by deleting the resource from the local index
									- removals are undone by trying to retrieve the sources that wer eremoved with a get from the server				
								- process all the events from the new change log starting with this event (just as in A.)
						  
						
					
					- poll regularly the available TRS providers
					- whenever a new change event is detected
						- update local copy accordingly
							- CREATE
								- query for the new resource
								- add to underlying database
							- MODify
								- Update the database with the content of the update query
							- DELETE
								- remove the current resource
					
					- need to understand the recommandations from the TRS spec for clients first.									
						- When the Base is empty, the Change Log describes a history of how the 
						Resource Set has grown and evolved since its inception. When the Change Log
						is empty, the Base is a simple enumeration of the Resources in the Resource Set.
						
						- There MUST NOT be a gap between the Base portion and the Change Log portion 	
						of a Tracked Resource Set representation; however, the Change Log portion 
						may contain earlier Change Event entries that would be accounted for by the 
						Base portion. A cutoff property of the Base identifies the point in the 
						Change Log at which processing of Change Events can be cut off because 
						older changes are already covered by the Base portion.
						
						- A typical Client will periodically poll the Tracked Resource Set looking 
						for recent Change Events. In order to cater to this usage, the Tracked Resource
						Sets representation MUST contain the triples for the referenced Change Log 
						(i.e., via a Blank Node, or an inline named Resource).  ( IS the triples
						for the change events meant here ???)
						
						- The URI of a Change Event MUST be guaranteed unique, even in the wake of a 
						Server rollback where sequence numbers get reused. ??
						
						- A Change Log represents a series of changes to its corresponding 
						Resource Set over some period of time. The Change Log MUST contain 
						Change Events for every Resource creation, deletion, and modification 
						during that period. ( if this period is not precise and shown on the TRS or
						change log why the MUST ) ?
						
						- A Server MUST report a Resource modification event if a GET on it would 
						return a semantically different response from previously.( meant would not ??!)
						
						- The Server SHOULD NOT report unnecessary Change Events although it might 
						happen, for example, if changes occur while the base is being computed. A 
						Client SHOULD ignore a creation event for a Resource that is already a member
						of the Resource Set, and SHOULD ignore a deletion or modification event for 
						a Resource that is not a member of the Resource Set. ( ? )
						
						- Because of the highly dynamic nature of the Resource Set, a Server may have
						difficulty enumerating the exact set of resources at a point in time. Because
						of that, the Base can be only an approximation of the Resource Set. A Base might
						omit mention of a Resource that ought to have been included or include a Resource
						that ought to have been omitted. For each erroneously reported Resource in the 
						Base, the Server MUST at some point include a corrective Change Event in the 
						Change Log more recent that the base cutoff event. The corrective Change Event 
						corrects the picture for that Resource, allowing the Client to compute the 
						correct set of member Resources. 
						
						- The corrective Change Event corrects the picture for that Resource, allowing 
						the Client to compute the correct set of member Resources. A corrective Change 
						Event might not appear in the Change Log that was retrieved when the Client 
						dereferenced the Tracked Resource Set URI. The Client might only see a corrective 
						Change Event when it processes the Change Log resource obtained by dereferencing 
						the Tracked Resource Set URI on later occasions. (??)
						
						
						
						- Client dereferencing the Tracked Resource Set URI ??
						
						- 

			- service provided
				- & or OR ?
					- OSLC Automation service
						- supporting post requests of new automation requests
					- OSLC Query base
						- supporting post requests of RDF SPARQL queries
						- supporting GET requests using OSLC where Query syntax ?
			
		
			
			
misc:
	- yeah seem to have an interesting example of how RELM supports ISO26262 obligations. it would
	b interesting to see whether that's an example provided with RELM to check the ideas in there
	
	- Scenarios
	
		- SPARQL as an update language
			- a little more knowledge would be needed about RDF in general to be able to use
			this. but in general the basic capability of deleting triples and updating them in
			the underlying RDF graph seems to be sufficient for our needs.
		- CM and AM providers traceability
			
			- Unfortunately there doesn't seem to be any kind of linking defined by either specification
			specifically for the purpose of tracing the artifacts of the one to the other. In AM
			there is the freedom to define some link type and to use it from AM resources.
			
			- one solution we could live with for now for demonstration purposes is the usage of one
			of the CM request links as a link to the resources from the simulink world
			
			
			
	- comparing OSLC query language with SPARQL
		
		- The OSLC query language
			- is more focused on patterns that address the following criterias
			in the returned results
			- filtering resources being returned
				- text search to restrict the elements to be returned in the queried resource
				- filter according to property values
			- presentation of the returned results
				- what kind of properties to return in the queried resource
				- what kind of properties to return in the resources contained in the queried resource
				- how should the resources returned in the query be ordered
			- Compared to SPARQL this is not a full fleged query language enablign as much read 
			 access as write access and features such as db transactions
	
		- SPARQL
			- support for database transactions for complex read write operations
			- ability to formulate complex operations about triple patterns to be matched
				- UNION clause e.g.
			- named graphs inside a dataset matching.
			- natively supported by third party software providers Vs the implementation effort needed
			to support oslc syntax
		
		- OSLC Vs Sparql
			- the ordering of the elements queried using oslc query language is limited while custom functions
			on the ordering of results according to some variable can be done in SPARQL
			- from OSLC query spec:
				- The query parameters defined here are intended to satsify simple query requirements,
				and be easy to both use and implement. More complex query requirements should be 
				satisfied using a full query language, e.g. SQL or SPARQL.
			- if we want to rely on existing web applicaiton/server implmentation of triplestores
			we can only hope that the extension of such applications is relatively easy towards
			additional support of OSLC query language.
			- SPARQL allows to limit the number of results returned by a query no such construct in
			 OSLC query language. This is probably intended to be covered by the paging specification
			 from the OSLC Core.
		
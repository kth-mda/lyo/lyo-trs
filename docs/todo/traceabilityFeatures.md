- Bugzilla
	- supports addition of links from
	the CM request to the comment field of the bug
		- "Affected by Defect"
		"Affects Plan Item"
		"Affects Requirement"
		"Implements Requirement"
		"Related Change Request""Tracks Change Set"
		"Tracks Requirement"
		- MArked as depreciated by the new OSLC CM spec, but still supported here
		"Tested by Test Case"
		"Affects Test Result"
		"Blocks Test Execution Record"
		"Related Test Execution Record"
		"Related Test Plane"
		"Related Test Script"
		
	- Effort needed to achieve the traceabiltiy
		- check the functionality by testing it and try adding all the links that can be contained
		in a CM Request. ( 2h)
		- for GET requests: implementation of retrieval of these links from 
		 the bug comment field and adding them to 
		 the OSLC CM links fields ( 2h)
		- for PUT Requests: synchronization of existing
		links with the ones coming in the update request ( 1h)
	
- Simulink
	- no traceability support for the moment
	- no traceability links in the resource shapes until now
	- Effort needed to achieve the traceability
		- need to implement the update service for updating existing elements in the adapter
		i would choose as candaidates for now
			- blocks
			- lines ( 30 min)
		- Need to extend existing m file export to ecore to support the export of the custom parameter
		we will use in simulink to the emf model representing the matlab model (2h min)
		- need additional matlab files to reflect the modifications on the underlying models
			- two alternatives after that
				- either reload the models (costly performance wise)
				- reflect the changes in the ecore representation ( little more effort but shouldn't be hard) (1h )
		- use custom properties field to fill in the link entries loaded from the emf matlab model
		adaptation process needs to be extended accordingly. (1h)
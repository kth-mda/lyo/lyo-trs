/**
 * *******************************************************************************
 *  Copyright (c) 2015 OFFIS e.V and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     OFFIS e.V - initial API and implementation
 *
 * *******************************************************************************
 */
package org.eclipse.lyo.oslc4j.trs.consumer;

/**
 * Thrown when the required representation can not be retrieved from the server.
 * Normally thrown by a Base member handler or a change event handler
 *
 * @author Omar
 *
 */
public class RepresentationRetrievalException extends Exception {
    static String message = "The representation of one of the resources could not be retireved while procesing the tracked resource set !";
    public RepresentationRetrievalException() {
        super(message);
    }

    public RepresentationRetrievalException(String string) {
        super(message + "\n" + string);
    }

    /**
     *
     */
    private static final long serialVersionUID = -5190311252768510792L;

}

/**
 * *******************************************************************************
 *  Copyright (c) 2015 OFFIS e.V and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     OFFIS e.V - initial API and implementation
 *
 * *******************************************************************************
 */
package org.eclipse.lyo.oslc4j.trs.consumer;

/**
 * Thrown normally by a TRS Provider when the last change event which was
 * processed can not be found in the list of change logs which was retrieved
 * from the server meaning probably that the server has been rolled back to a
 * state previous to that change event
 *
 * @author Omar
 *
 */
public class ServerRollBackException extends Exception {

    public ServerRollBackException(String string) {
        super(string);
    }

    /**
     *
     */
    private static final long serialVersionUID = -5190311252768510792L;

}

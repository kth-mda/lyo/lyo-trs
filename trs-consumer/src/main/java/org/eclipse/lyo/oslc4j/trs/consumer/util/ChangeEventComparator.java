/**
 * *******************************************************************************
 *  Copyright (c) 2015 OFFIS e.V and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     OFFIS e.V - initial API and implementation
 *
 * *******************************************************************************
 */
package org.eclipse.lyo.oslc4j.trs.consumer.util;

import java.util.Comparator;

import org.eclipse.lyo.core.trs.ChangeEvent;

/**
 * Comparator for ordering change events
 *
 * @author Omar
 *
 */
public class ChangeEventComparator implements Comparator<ChangeEvent> {
    @Override
    public int compare(ChangeEvent ce1, ChangeEvent ce2) {
        return ce1.getOrder() - ce2.getOrder();
    }
}

package org.eclipse.lyo.oslc4j.trs.consumer.test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.eclipse.lyo.oslc4j.trs.consumer.rdf.RdfUtil;
import org.eclipse.lyo.oslc4j.trs.consumer.sparql.SparqlUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.TDBLoader;
import com.hp.hpl.jena.tdb.sys.TDBInternal;
import com.hp.hpl.jena.update.UpdateAction;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateRequest;

import junit.framework.Assert;

public class SparqlUtilTest {

    static Logger logger = Logger.getLogger(SparqlUtilTest.class);

    static Dataset dataset;

    private static void clear() throws URISyntaxException {
        dataset.begin(ReadWrite.WRITE);

        String sparqlUpdate = "DROP ALL";
        UpdateRequest updateReq = UpdateFactory.create(sparqlUpdate);
        try {
            UpdateAction.execute(updateReq, dataset);
        } catch (Exception e) {
            logger.error(e);
        }
        dataset.commit();
        dataset.end();
    }

    private void executeUpdate(String sparqlUpdate) throws URISyntaxException {
        dataset.begin(ReadWrite.WRITE);
        UpdateRequest updateReq = UpdateFactory.create(sparqlUpdate);
        try {
            UpdateAction.execute(updateReq, dataset);
        } catch (Exception e) {
            logger.error(e);
        }
        dataset.commit();
        dataset.end();

    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        String dataSetPath = SparqlUtilTest.class.getResource("/test_data_base/test_data_set").getFile();
        com.hp.hpl.jena.tdb.base.file.Location directory = new com.hp.hpl.jena.tdb.base.file.Location(dataSetPath);
        dataset = TDBFactory.createDataset(directory);
        clear();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        dataset.close();
    }

    @Before
    public void setUp() throws Exception {

        dataset.begin(ReadWrite.WRITE);
        InputStream elvisModel = SparqlUtilTest.class.getResourceAsStream("/test_data_base/elvisimp.rdf");
        try {
            TDBLoader.load(TDBInternal.getDatasetGraphTDB(dataset.asDatasetGraph()), elvisModel, true);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        dataset.commit();
        dataset.end();
    }

    @After
    public void tearDown() throws Exception {
        clear();
    }

    @Test
    public final void testCreateGraphQuery() {
        String graphName = "http://graph_whateveuuuur.com";
        dataset.begin(ReadWrite.READ);
        Model defaultModel = dataset.getDefaultModel();
        String defaultModelNTrip = null;
        try {
            defaultModelNTrip = RdfUtil.modelToNTriple(defaultModel);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.commit();
        dataset.end();
        try {
            String sparqlUpdate = SparqlUtil.getModificationEventQuery(graphName,
                    defaultModelNTrip);
            executeUpdate(sparqlUpdate);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.begin(ReadWrite.READ);
        Assert.assertTrue(dataset.containsNamedModel(graphName));
        dataset.commit();
        dataset.end();
    }

    @Test
    public final void testDropGraphQuery() {
        String graphName = "http://graph_whateveuuuur.com";
        dataset.begin(ReadWrite.READ);
        Model defaultModel = dataset.getDefaultModel();
        String defaultModelNTrip = null;
        try {
            defaultModelNTrip = RdfUtil.modelToNTriple(defaultModel);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.commit();
        dataset.end();
        try {
            String sparqlUpdate = SparqlUtil.getModificationEventQuery(graphName, defaultModelNTrip);
            executeUpdate(sparqlUpdate);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.begin(ReadWrite.READ);
        Assert.assertTrue(dataset.containsNamedModel(graphName));
        dataset.commit();
        dataset.end();
        try {
            String sparqlUpdate = SparqlUtil.dropGraphQuery(graphName);
            executeUpdate(sparqlUpdate);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.begin(ReadWrite.READ);
        Assert.assertTrue(!dataset.containsNamedModel(graphName));
        dataset.commit();
        dataset.end();
    }
    //
    @Test
    public final void testRemoveAllTriplesInGraphQuery() {
        String graphName = "http://graph_whateveuuuur.com";
        dataset.begin(ReadWrite.READ);
        Model defaultModel = dataset.getDefaultModel();
        String defaultModelNTrip = null;
        try {
            defaultModelNTrip = RdfUtil.modelToNTriple(defaultModel);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.commit();
        dataset.end();
        try {
            String sparqlUpdate = SparqlUtil.getModificationEventQuery(graphName, defaultModelNTrip);
            executeUpdate(sparqlUpdate);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.begin(ReadWrite.READ);
        Assert.assertTrue(dataset.containsNamedModel(graphName));
        dataset.commit();
        dataset.end();
        try {
            String sparqlUpdate = SparqlUtil.removeAllTriplesInGraphQuery(graphName);
            executeUpdate(sparqlUpdate);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }

        dataset.begin(ReadWrite.READ);
        Assert.assertFalse(dataset.containsNamedModel(graphName));
        dataset.commit();
        dataset.end();
    }

    @Test
    public final void testAddTriplesToGraphQueryStringModel() {
        String graphName = "http://graph_whateveuuuur.com";
        dataset.begin(ReadWrite.READ);
        Model defaultModel = dataset.getDefaultModel();
        String defaultModelNTrip = null;
        try {
            defaultModelNTrip = RdfUtil.modelToNTriple(defaultModel);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.commit();
        dataset.end();
        try {
            String sparqlUpdate = SparqlUtil.getModificationEventQuery(graphName, defaultModelNTrip);
            executeUpdate(sparqlUpdate);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }

        String statement = "<http://www.w3.org/People/EM/contact#me> <http://www.w3.org/2000/10/swap/pim/contact#fullName> \"Eric Miller\" .";

        try {
            String sparqlUpdate = SparqlUtil.addTriplesToGraphQuery(graphName, statement);
            executeUpdate(sparqlUpdate);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.begin(ReadWrite.READ);
        try {
            Model namedModel = dataset.getNamedModel(graphName);
            String namedModelNtriples = RdfUtil.modelToNTriple(namedModel);
            Assert.assertTrue(namedModelNtriples.contains(statement));
        } catch (IOException e) {
            logger.error(e);
        }

        dataset.commit();
        dataset.end();

    }

    @Test
    public final void testGetModificationEventQueryChangeEventModel() {
        String graphName = "http://graph_whateveuuuur.com";
        dataset.begin(ReadWrite.READ);
        Model defaultModel = dataset.getDefaultModel();
        String defaultModelNTrip = null;
        try {
            defaultModelNTrip = RdfUtil.modelToNTriple(defaultModel);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }
        dataset.commit();
        dataset.end();
        try {
            String sparqlUpdate = SparqlUtil.getModificationEventQuery(graphName, defaultModelNTrip);
            executeUpdate(sparqlUpdate);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            logger.error(e);
            Assert.assertFalse(true);
        }


        dataset.begin(ReadWrite.READ);
        try {
            Model namedModel = dataset.getNamedModel(graphName);
            String namedModelNtriples = RdfUtil.modelToNTriple(namedModel);
            Assert.assertTrue(dataset.containsNamedModel(graphName));
            Assert.assertTrue(namedModelNtriples.equals(defaultModelNTrip));
        } catch (IOException e) {
            logger.error(e);
        }

        dataset.commit();
        dataset.end();
    }
    //
    // @Test
    // public final void testAddTriplesToGraphQueryStringString() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testGetChangeEventQuery() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    //
    // @Test
    // public final void testGetModificationEventQueryChangeEventString() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testGetModificationEventQueryStringString() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testGetCreationEventQuery() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testGetDeletionEventQuery() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testCreateGraph() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testDropGraph() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testAddTriplesToNamedGraph() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testRemoveAllTriplesInNamedGraph() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testProcessChangeEvent() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testProcessQuery() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testProcessQuery_sesameStringStringStringString() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testProcessQuery_sesameStringRepositoryConnection() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testGetRepoConnectionStringStringString() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testGetRepoConnectionStringStringStringString() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testEvalQueryStringStringStringString() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testEvalUpdate() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testEvalQueryRepositoryConnectionString() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testAppendSparqldQuery() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testProcessTripleAdditionQuery() {
    // fail("Not yet implemented"); // TODO
    // }
    //
    // @Test
    // public final void testLinkTriple() {
    // fail("Not yet implemented"); // TODO
    // }

}

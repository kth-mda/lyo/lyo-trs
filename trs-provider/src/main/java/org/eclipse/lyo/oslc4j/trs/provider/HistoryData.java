/*******************************************************************************
 * Copyright (c) 2012, 2014 IBM Corporation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * * Contributors:
 *
 *    Hirotaka Matsumoto - Initial implementation
 *    OFFIS e.V -
 *******************************************************************************/

package org.eclipse.lyo.oslc4j.trs.provider;

import java.net.URI;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * This class represents an event occuring on a specific resource at some point
 * in time.
 *
 * @author Omar
 *
 */
public class HistoryData {
    final public static String CREATED = "Created"; // trs:Creation //$NON-NLS-1$
    final public static String MODIFIED = "Modified"; // trs:Modification //$NON-NLS-1$
    final public static String DELETED = "Deleted"; // trs:Modification //$NON-NLS-1$

    private Date timestamp; // Time stamp;
    private URI uri; // OSLC link
    private String type = CREATED; // trs:Creation , trs:Modification or
    // trs:Deletion

    private HistoryData() {
    }

    public static HistoryData getInstance(Date timestamp, URI uri, String type) {
        HistoryData h = new HistoryData();
        h.timestamp = timestamp;
        h.uri = uri;
        h.type = type;
        return h;
    }

    /**
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @return the url
     */
    public URI getUri() {
        return uri;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Type=" + getType() + ", " + //$NON-NLS-1$ //$NON-NLS-2$
                "Timestamp=" + getTimestamp() + ", " + //$NON-NLS-1$ //$NON-NLS-2$
                "URI=" + getUri() + "\n";//$NON-NLS-1$ //$NON-NLS-2$
    }

    static Logger logger = Logger.getLogger(HistoryData.class);
}
